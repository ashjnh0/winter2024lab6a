public class Card {
	
	private int rank;
	private String suit;
	
	public Card(int rank, String suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public String toString() {
		if (value == 1) {
			return "Ace of " + suit;
		} else if (value == 11) {
			return "Jack of " + suit;
		} else if (value == 12) {
			return "Queen of " + suit;
		} else if (value == 13) {
			return "King of " + suit;
		} else {
			return value + " of " + suit;
		}
	}
}