import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(system.in);

		Deck d = new Deck();
		d.shuffle();
		
		System.out.println("Hello There :) Welcome to the card game!");
		System.out.println("You have " + d.length() + "number of cards" );

		
		int cardsToRemove = -1;
		boolean validInput = false;

		while(cardsToRemove < 0 || cardsToRemove > d.length()) {
			System.out.println("How many cards would you like to remove?");
			cardsRemoved = scanner.nextInt();
			if(cardsToRemove >= 0 && cardsToRemove <= d.length()) {
				validInput = true;
			}
			else {
				System.out.println("Enter a valid number of cards (0-" + d.length() + ")" );
			}
		}

		for(int i = 0; i < cardsRemoved; i++) {
			d.drawTopCard();
		}

		System.out.println("Here are the number of cards after removing: " + d.length());
		d.shuffle();
		System.out.println(d);
		scanner.close();
	}
}